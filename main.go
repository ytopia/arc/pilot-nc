package main

import (
	"fmt"
	"os"
	"net"
	"flag"
	"strings"
)

func main() {
  var host string
	var port string
  flag.Parse()
	if flag.NArg() != 2 {
		fmt.Println("Usage: pilot-nc host port")
		os.Exit(1)
	}
  host = flag.Arg(0)
  port = flag.Arg(1)
  _, err := net.Dial("tcp", strings.Join([]string{host, port}, ":"))
	if err != nil {
		os.Exit(1)
	}
}
