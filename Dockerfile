ARG GOLANG_VERSION=latest
FROM golang:$GOLANG_VERSION as builder

WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o=pilot-nc .

FROM scratch
COPY --from=builder /app/pilot-nc /

CMD ["/pilot-nc"]